Halo i bims, die readme

Mindestanforderungen:
Java 8 installiert.
900x700px Bildschirm.

Mangels alternativer Testumgebungen kann nur eine 
fehlerfreie Ausfuehrung unter Windows 10 gewaehrleistet werden.
Trotzdem ist eine UNIX Unterstützung implementiert.

Installationshinweise

Ausfuehrung der JAR:
per Console: java -jar pfad/zur/jar
per Doppelklick: direkter Start

Import des Projektes in eine IDE:
Der Import des Projektes erfolgt nach den ueblichen Schritten der jeweiligen IDE.
Es ist darauf zu achten, dass Java 8 als Plattform ausgewaehlt wird,
sowie, dass der src Ordner als Source Ordner deklariert wird.
Desweiteren ist der resources Ordner im Stammpfad als Resource Pfad auszuwaehlen.
