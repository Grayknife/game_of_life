package game.service;

import java.util.ArrayList;

/**
 * Created by andre on 19.09.2017.
 */
public class TextReader {
	/**
	 * Konvertiert den Eingabe-Text in ein zweidimensionales Array vom Typ
	 * Boolean und gibt es zur�ck. Der Eingabetext muss im .RLE-Format
	 * vorliegen.
	 * 
	 * @param text
	 * @return Array[][] vom Typ Boolean
	 */
	public static boolean[][] convertTextToWorld(String text) {
		ArrayList<ArrayList<Boolean>> listRows = new ArrayList<>();
		ArrayList<Boolean> listColumValue = new ArrayList<>();
		char[] chararray = text.toCharArray();
		int maxrowlength = 0;
		int lastCharCount = 0;
		boolean ignoreLine = false;
		for (char c : chararray) {
			if (!ignoreLine) {
				if (c == '#' || c == 'x') {
					ignoreLine = true;
				} else if (c == 'b') {
					do {
						listColumValue.add(false);
					} while (--lastCharCount > 0);
					lastCharCount = 0;
				} else if (c == 'o') {
					do {
						listColumValue.add(true);
					} while (--lastCharCount > 0);
					lastCharCount = 0;
				} else if (c == '$') {
					do {
						listRows.add(listColumValue);
						if (listColumValue.size() > maxrowlength) {
							maxrowlength = listColumValue.size();
						}
						listColumValue = new ArrayList<>();
					} while (--lastCharCount > 0);
					lastCharCount = 0;
				} else if (c > 47 && c < 58) {
					lastCharCount = lastCharCount * 10;
					lastCharCount = lastCharCount + Character.getNumericValue(c);
				} else if (c == '!') {
					break;
				}
			} else if (c == '\n') {
				ignoreLine = false;
			}
		}
		if (listColumValue.size() > maxrowlength) {
			maxrowlength = listColumValue.size();
		}
		listRows.add(listColumValue);

		boolean[][] world = new boolean[listRows.size()][maxrowlength];
		for (int y = 0; y < world.length; y++) {
			for (int x = 0; x < listRows.get(y).size(); x++) {
				world[y][x] = listRows.get(y).get(x);
			}
		}
		return world;
	}

	/**
	 * Konvertiert das Eingabe Array[][] vom Typ Boolean in einen String. Der
	 * String ist im .RLE-Format.
	 * 
	 * @param world
	 * @return String
	 */
	static String ConvertWorldToString(boolean[][] world) {
		StringBuilder sb = new StringBuilder();
		int lastCharCount;
		boolean lastCharType;
		for (int y = 0; y < world.length; y++) {
			lastCharCount = 0;
			lastCharType = false;
			for (int x = 0; x < world[y].length; x++) {
				if (world[y][x] == lastCharType) {
					lastCharCount++;
				} else {
					if (lastCharCount > 1) {
						sb.append(lastCharCount);
					}
					if (lastCharCount > 0) {
						if (lastCharType) {
							sb.append('o');
						} else {
							sb.append('b');
						}
					}
					lastCharType = world[y][x];
					lastCharCount = 1;
				}
			}
			if (lastCharCount > 1) {
				sb.append(lastCharCount);
			}
			if (lastCharType) {
				sb.append('o');
			} else {
				sb.append('b');
			}
			sb.append('$');
			sb.append(System.lineSeparator());
		}
		sb.append('!');
		return sb.toString();
	}
}
