package game.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class PatternLoader {

	/**
	 * Liest die vordefinierten Pattern ein und gibt sie in einer Liste vom Typ
	 * String zurueck.
	 * 
	 * @return List vom Typ String
	 * @param folderNames
	 */
	public static List<List<String>> loadPredefinedPatternList(List<String> folderNames) {
		FileSystem fileSystem;
		List<List<String>> predefinedPatternLists = new ArrayList<>();

		for (int i = 0; i < folderNames.size(); i++) {
			predefinedPatternLists.add(new ArrayList<>());
			try {
				URL resource = PatternLoader.class.getClass().getResource("/Patterns/" + folderNames.get(i));
				if (resource == null) {
					if (folderNames.get(i).contains("Custom")) {
						predefinedPatternLists.get(i).addAll(listSavedPattern());
					}
					continue;
				}
				URI uri = resource.toURI();
				Path myPath;
				if (uri.getScheme().equals("jar")) {
					// within jar
					if (i == 0) {
						fileSystem = FileSystems.newFileSystem(uri, Collections.emptyMap());
					} else {
						fileSystem = FileSystems.getFileSystem(uri);
					}
					myPath = fileSystem.getPath("/Patterns/" + folderNames.get(i));
				} else {
					// within ide
					myPath = Paths.get(uri);
				}
				predefinedPatternLists.get(i).addAll(Files.list(myPath)
						.filter(f -> f.getFileName().toString().contains("."))
						.map(l -> l.getFileName().toString().substring(0, l.getFileName().toString().lastIndexOf('.')))
						.sorted().collect(toList()));

			} catch (IOException ignored) {

			} catch (URISyntaxException e) {
				e.printStackTrace();
			}

		}
		return predefinedPatternLists;
	}

	/**
	 * Mit dem Eingabestring sucht die Methode die passende .RLE-Datei und
	 * konvertiert sie in ein zweidimensionales Array vom Typ Boolean.
	 * 
	 * @param patternname
	 * @return Array[][] vom Typ Boolean
	 */
	public static boolean[][] loadPattern(String patternname) {
		boolean[][] world = new boolean[0][];
		try {
			InputStream is = PatternLoader.class.getClass().getResourceAsStream("/Patterns/" + patternname + ".rle");
			if (is != null) {
				StringBuilder sb = new StringBuilder();
				while (true) {
					int c = is.read();
					if (c == -1) {
						break;
					}
					sb.append((char) c);
				}
				is.close();
				world = TextReader.convertTextToWorld(sb.toString());
			} else {
				if (patternname.contains("Custom")) {
					world = loadWorldFromSavedPattern(patternname);
				}
			}
		} catch (IOException ignored) {

		}
		return world;
	}

	/**
	 * Speichert ein zweidimensionales Array vom Typ Boolean in eine .RLE-Datei
	 * in einem Ordner auf der Festplatte. Der Name der Datei wird mit
	 * uebergeben.
	 * 
	 * @param world
	 * @param name
	 * @return boolean True, wenn das Speichern erfolgreich verlief, sonst False
	 */
	public static boolean savePattern(boolean[][] world, String name) {
		if (name == null || name.length() < 1) {
			return false;
		}
		String filepath = getAppDataFilepath();
		filepath += name + ".rle";
		try {
			Path pathToFile = Paths.get(filepath);
			Files.createDirectories(pathToFile.getParent());
			if (!Files.exists(pathToFile)) {
				Files.createFile(pathToFile);
			}
			BufferedWriter writer = Files.newBufferedWriter(pathToFile, StandardCharsets.UTF_8);

			writer.write(TextReader.ConvertWorldToString(world));
			writer.close();

		} catch (IOException ignored) {
			return false;
		}
		return true;
	}

	private static List<String> listSavedPattern() {
		String filepath = getAppDataFilepath();
		try {
			Path pathToFile = Paths.get(filepath);
			Files.createDirectories(pathToFile.getParent());
			return Files.walk(pathToFile, 1).filter(f -> f.getFileName().toString().contains("."))
					.map(l -> l.getFileName().toString().substring(0, l.getFileName().toString().lastIndexOf('.')))
					.sorted().collect(toList());

		} catch (IOException ignored) {

		}
		return new ArrayList<>();
	}

	private static boolean[][] loadWorldFromSavedPattern(String patternname) {
		if (patternname == null || patternname.length() < 1) {
			return new boolean[1][1];
		}
		String filepath = getAppDataFilepath();
		String[] split = patternname.split("/");
		if (split.length == 2) {
			filepath += split[1] + ".rle";
		}
		try {
			Path pathToFile = Paths.get(filepath);
			Files.createDirectories(pathToFile.getParent());
			if (!Files.exists(pathToFile)) {
				return new boolean[1][1];
			}
			BufferedReader reader = Files.newBufferedReader(pathToFile, StandardCharsets.UTF_8);
			StringBuilder sb = new StringBuilder();
			while (true) {
				int c = reader.read();
				if (c == -1) {
					break;
				}
				sb.append((char) c);
			}
			reader.close();
			return TextReader.convertTextToWorld(sb.toString());

		} catch (IOException ignored) {

		}
		return new boolean[1][1];
	}

	private static String getAppDataFilepath() {
		String filepath = null;
		if (System.getProperty("os.name").toLowerCase().contains("windows")) {
			filepath = System.getenv("APPDATA");
		} else {
			filepath = System.getProperty("user.home");
		}
		filepath += "/.game_of_life/Patterns/";
		return filepath;
	}

}