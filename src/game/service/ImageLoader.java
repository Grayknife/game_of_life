package game.service;

import game.model.MainModel;
import javafx.stage.FileChooser;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

public class ImageLoader {

	private int scaledWidth;
	private int scaledHeight;
	private BufferedImage blackWhite;
	private MainModel mainModel;

	/**
	 * 
	 * @param mainModel
	 */
	public ImageLoader(MainModel mainModel) {
		this.mainModel = mainModel;
	}

	/**
	 * Das Bild, welches in schwarz-weiss-monochrom vorliegt, wird Pixel fuer
	 * Pixel abgestastet und davon abhaengig jede Zello tot oder lebendig
	 * gesetzt.
	 * 
	 * @return Array[][] vom Typ Boolean
	 */
	private boolean[][] convertImageIntoWorld() {
		boolean[][] world = new boolean[scaledHeight][scaledWidth];
		for (int x = 0; x < scaledWidth; x++) {
			for (int y = 0; y < scaledHeight; y++) {
				world[y][x] = computeBoolean(blackWhite.getRGB(x, y));
			}
		}
		return world;
	}

	/**
	 * Berechnet ob die Zelle lebt oder tot ist, anhand der Eingabe-Color.
	 * 
	 * @param color
	 * @return Boolean
	 */
	private boolean computeBoolean(int color) {
		switch (color) {
		case -16777216:
			return true;
		case -1:
			return false;
		default:
			return false;
		}
	}

	/**
	 * Public Methode, um ein Bild zu laden. Alle weiteren Methoden zur
	 * Bildweiterverarbeitung werden hier aufgerufen.
	 * 
	 * @return Array[][] vom Typ Boolean
	 */
	public boolean[][] loadImage() {
		chooseFileAndLoad();
		return convertImageIntoWorld();
	}

	/**
	 * Oeffnet den Dateiexplorer des Benutzers um ein Bild zu laden. M�glich
	 * sind .png-,.jpg- und .gif-Dateien. Bei korrektem Input wird die Methode
	 * readImage aufgerufen.
	 */
	private void chooseFileAndLoad() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open image.");
		FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif");
		fileChooser.getExtensionFilters().add(filter);

		boolean validImageFound;
		do {
			final File file = fileChooser.showOpenDialog(null);

			if (file == null) {
				break;
			}
			validImageFound = readImage(file);

			if (!validImageFound) {
				continue;
			}
		} while (!validImageFound);
	}

	/**
	 * Das Eingabebild wird gescheit proportioniert und in
	 * schwarz-weiss-monochrom konvertiert. Bei erfolgreichem Bearbeiten wird
	 * true zurueckgegeben.
	 * 
	 * @param file-image
	 * @return Boolean
	 */
	private boolean readImage(File file) {
		try {
			BufferedImage master = ImageIO.read(file);
			if (master == null) {
				return false;
			}
			blackWhite = new BufferedImage(master.getWidth(), master.getHeight(), BufferedImage.TYPE_BYTE_BINARY);
			double proportion = 1.0 * blackWhite.getWidth() / blackWhite.getHeight();

			scaledWidth = mainModel.getSizeX() - 1;
			scaledHeight = (int) (scaledWidth / proportion);
			if (scaledHeight > mainModel.getSizeY()) {
				scaledHeight = mainModel.getSizeY() - 1;
				scaledWidth = (int) (scaledHeight * proportion);
			}
			if (scaledWidth > master.getWidth()) {
				scaledWidth = master.getWidth();
			}
			if (scaledHeight > master.getHeight()) {
				scaledHeight = master.getHeight();
			}

			Graphics2D g2d = blackWhite.createGraphics();
			g2d.drawImage(master, 0, 0, scaledWidth, scaledHeight, null);
			g2d.dispose();

		} catch (IOException ex) {
			ex.printStackTrace();
			return false;
		}
		return true;
	}

}
