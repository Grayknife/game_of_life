package game.controller;

import game.logic.WorldEvolver;
import game.model.MainModel;
import game.model.DrawingModel;
import game.model.Tuple;
import game.service.ImageLoader;
import game.service.PatternLoader;
import game.service.TextReader;
import game.view.AboutDialog;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.transformation.FilteredList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.Optional;

public class Controller {

	@FXML
	private StackPane stackPane;

	@FXML
	private Button btn_start;

	@FXML
	private Button btn_stop;

	@FXML
	private Button btn_reset;

	@FXML
	private Button btn_load;

	@FXML
	private Slider sld_zoom;

	@FXML
	private Slider sld_speed;

	@FXML
	private Canvas can_draw;

	@FXML
	private TextArea txt_area;

	@FXML
	private Label txt_zoom;

	@FXML
	private Label txt_speed;

	@FXML
	private Label txt_generation;

	@FXML
	private Label txt_population;

	@FXML
	private Label txt_size;

	@FXML
	private Menu menu_select_algorithm;

	@FXML
	private MenuBar menubar;

	private MainModel mainModel;
	private WorldEvolver algorithm;
	private boolean evolutionTaskCancelled;
	private int speed;
	private int lastMouseToggledX;
	private int lastMouseToggledY;
	private CanvasRedrawTask<DrawingModel> canvasRedrawTask;
	private RadioMenuItem lastSuccessfulItem;

	public Controller() {
		mainModel = new MainModel();
		algorithm = new WorldEvolver(mainModel);
		speed = 100;
		evolutionTaskCancelled = true;
		lastMouseToggledX = -1;
		lastMouseToggledY = -1;
	}

	/**
	 * Startbutton fuer Evolution.
	 */
	@FXML
	private void btn_start_pressed() {
		btn_start.setDisable(true);
		btn_load.setDisable(true);
		txt_area.setDisable(true);
		menubar.getMenus().forEach(m -> m.setDisable(true));
		evolutionTaskCancelled = false;
		mainModel.setIsRunning(true);
		Task<Void> evolutionTask = new Task<Void>() {
			@Override
			protected Void call() throws Exception {
				while (!evolutionTaskCancelled) {
					mainModel.forceWorld(algorithm.runAlgorithm());
					canvasRedrawTask.requestRedraw(mainModel.getDrawingModel(false));
					Platform.runLater(() -> updateUILabels());
					try {
						Thread.sleep(speed);
					} catch (InterruptedException ignored) {
					}
				}
				return null;
			}
		};
		evolutionTask.setOnSucceeded(event -> finishedEvolutionTask());
		new Thread(evolutionTask).start();
	}

	/**
	 * Stoppt die Evolution
	 */
	@FXML
	private void btn_stop_pressed() {
		evolutionTaskCancelled = true;
		btn_start.setDisable(false);
		btn_load.setDisable(false);
		txt_area.setDisable(false);
		menubar.getMenus().forEach(m -> m.setDisable(false));
	}

	/**
	 * Stellt die Canvas auf die Startwelt zur�ck
	 */
	@FXML
	private void btn_reset_pressed() {
		updateCanvas();
	}

	/**
	 * Konvertiert den Text aus der .RLE-Textarea in eine Welt.
	 */
	@FXML
	private void btn_load_pressed() {
		if (evolutionTaskCancelled) {
			loadWorld(TextReader.convertTextToWorld(txt_area.getText()));
		}
	}

	/**
	 * Loescht die aktuelle Welt und ersetzt sie durch eine leere Welt.
	 */
	@FXML
	private void itm_new_world_pressed() {
		if (evolutionTaskCancelled) {
			mainModel.forceWorld(new boolean[1][1]);
			updateCanvas();
		}
	}

	/**
	 * Speichert die aktuelle Welt in einer .RLE-Datei. Der Name ist wahelbar
	 * durch ein Dialogfenster.
	 */
	@FXML
	private void itm_save_world_pressed() {
		String patternName = nameDialog();
		boolean successful = PatternLoader.savePattern(mainModel.getWorld(), patternName);
		if (successful) {
			List<Menu> custom = menubar.getMenus().filtered(m -> m.getText().contains("Custom"));
			if (custom.size() > 0) {
				MenuItem menuItem = new MenuItem(patternName);
				menuItem.setOnAction(e -> {
					if (evolutionTaskCancelled) {
						loadWorld(PatternLoader
								.loadPattern(menuItem.getParentMenu().getText() + "/" + menuItem.getText()));

					}
				});
				custom.get(0).getItems().add(menuItem);
			}
		}
	}

	/**
	 * Bei Aufruf wird der Dateibrowser geoeffnet und ein Bild kann ausgewahelt
	 * werden. Das Bild wird in die Welt konvertiert.
	 */
	@FXML
	private void itm_load_image_pressed() {
		ImageLoader image = new ImageLoader(mainModel);
		boolean[][] world = image.loadImage();
		loadWorld(world);
	}

	/**
	 * Schliesst die Anwendung.
	 */
	@FXML
	private void itm_close_pressed() {
		evolutionTaskCancelled = true;
		Platform.exit();
	}

	/**
	 * Oeffnet ein neues Fenster, wo Informationen zur Anwendung stehen.
	 */
	@FXML
	public void itm_help_about_pressed() {
		AboutDialog aboutDialog = new AboutDialog();
		aboutDialog.showAndWait();
	}

	/**
	 * Nach der Initialisierung der UI Elemente in der fxml kann auf diese ab
	 * diesem Funktionsaufruf zugegriffen werden. Hier werden somit wichtige UI
	 * Elemente vorbereitet auf die Nutzung und vordefinierte Pattern geladen.
	 *
	 */
	@FXML
	private void initialize() {
		createRedrawTask();
		prepareUIHandlers();
		readPredefinedPattern();
	}

	/**
	 * Oeffnet ein Dialogfenster, wo ein Name fuer eine Welt eingegeben werden
	 * kann. Bei Abbruch wird null zurueckgegeben.
	 * 
	 * @return Name vom Typ String
	 */
	private String nameDialog() {
		TextInputDialog dialog = new TextInputDialog("World");
		dialog.setTitle("Save World");
		dialog.setHeaderText("Enter name for your world.");
		dialog.setContentText("Name:");
		Optional<String> result = dialog.showAndWait();
		return result.isPresent() ? result.get() : null;

	}

	/**
	 * Oeffnet ein Dialogfenster, wo eine Regel eingegeben werden kann. Bei
	 * Abbruch wird null zurueckgegeben.
	 * 
	 * @return Regel vom Typ String
	 */
	private String ownWorldDialog() {
		TextInputDialog dialog = new TextInputDialog("23/3");
		dialog.setTitle("Custom Rule");
		dialog.setHeaderText("Enter your custom Rule.\nPlease consider the scheme!\n");
		dialog.setContentText("Rule:");
		Optional<String> result = dialog.showAndWait();
		return result.isPresent() ? result.get() : null;

	}

	/**
	 * Diese Methode erstellt einen neuen Task vom Typ CanvasRedrawTask und
	 * definiert, welches Verhalten ein Aufruf von redraw(graphicsContext,
	 * drawingModel) bewirkt.
	 */
	private void createRedrawTask() {
		canvasRedrawTask = new CanvasRedrawTask<DrawingModel>(can_draw) {
			/**
			 * @param context
			 *            GraphicsContext, mit dem gearbeitet werden soll.
			 * @param data
			 *            DrawingModel, welches alle wichtigen Anweisungen
			 *            enthält, welche auf dem GraphicsContext ausgeführt
			 *            werden sollen.
			 */
			protected void redraw(GraphicsContext context, DrawingModel data) {
				if (data.clear) {
					context.clearRect(0, 0, can_draw.getWidth(), can_draw.getHeight());
					data.clear = false;

				}
				int zoom = mainModel.getZoom();
				while (!data.pointsToRedraw.isEmpty()) {
					Tuple<Integer, Integer, Integer> tpl = null;
					try {
						tpl = data.pointsToRedraw.take();
					} catch (InterruptedException e) {
						return;
					}
					// process each tuple
					switch (tpl.type) {
					case 0:
						context.setFill(Color.BURLYWOOD);
						break;
					case 1:
						context.setFill(Color.CHOCOLATE);
						break;
					case 2:
						context.setFill(Color.DARKBLUE);
						break;
					default:
						break;
					}
					context.fillRect(tpl.xValue * zoom, tpl.yValue * zoom, zoom - (zoom / 10.0), zoom - (zoom / 10.0));
				}
			}
		};
	}

	/**
	 * Sammelmethode für wichtige Anpassungen/Definitionen der in der fxml
	 * definierte UI Elemente.
	 */
	private void prepareUIHandlers() {
		// Bind canvas size to stack pane size.
		can_draw.widthProperty().bind(stackPane.widthProperty());
		can_draw.heightProperty().bind(stackPane.heightProperty());
		can_draw.widthProperty().addListener((ov, oldValue, newValue) -> {
			if (evolutionTaskCancelled) {
				updateCanvas();
			}
		});
		can_draw.heightProperty().addListener((ov, oldValue, newValue) -> {
			if (evolutionTaskCancelled) {
				updateCanvas();
			}
		});
		sld_zoom.valueProperty().addListener((observable, oldValue, newValue) -> {
			if (mainModel.getZoom() != newValue.intValue()) {
				setZoom(newValue.intValue());
			}

		});
		sld_speed.valueProperty().addListener((observable, oldValue, newValue) -> {
			speed = 500 / newValue.intValue();
			txt_speed.setText("Speed: " + newValue.intValue());
		});

		can_draw.addEventHandler(MouseEvent.MOUSE_PRESSED, event -> {
			int zoom = mainModel.getZoom();
			int x = (int) Math.round(event.getX()) / zoom;
			int y = (int) Math.round(event.getY()) / zoom;
			lastMouseToggledX = x;
			lastMouseToggledY = y;
			mainModel.worldToggleCell(x, y);
			canvasRedrawTask.requestRedraw(mainModel.getDrawingModel(false));
		});

		can_draw.addEventHandler(MouseEvent.MOUSE_DRAGGED, event -> {
			int zoom = mainModel.getZoom();
			int x = (int) Math.round(event.getX()) / zoom;
			int y = (int) Math.round(event.getY()) / zoom;
			if (lastMouseToggledX != x || lastMouseToggledY != y) {
				lastMouseToggledX = x;
				lastMouseToggledY = y;
				mainModel.worldToggleCell(x, y);
				canvasRedrawTask.requestRedraw(mainModel.getDrawingModel(false));
			}
		});
	}

	/**
	 * Laedt die vordefinierten Muster.
	 */
	private void readPredefinedPattern() {
		final TreeMap<String, String> algorithmMap = new TreeMap<>();
		algorithmMap.put("1-Conways World", "23/3");
		algorithmMap.put("2-Copy World", "1357/1357");
		algorithmMap.put("3-Exploding World", "236/3");
		algorithmMap.put("4-Labyrinth World", "12345/3");
		algorithmMap.put("5-Custom World", "");

		ToggleGroup toggleGroup = new ToggleGroup();
		for (String algorithmName : algorithmMap.keySet()) {
			RadioMenuItem menuItem = new RadioMenuItem(algorithmName);
			menuItem.setOnAction(e -> {
				if (evolutionTaskCancelled) {
					if (menuItem.getText().equals("5-Custom World")) {
						String rule = ownWorldDialog();
						if (rule != null) {
							algorithm.setAlgorithm(rule);
						} else {
							lastSuccessfulItem.setSelected(true);
							algorithm.setAlgorithm(algorithmMap.get(menuItem.getText()));
						}
					} else {
						algorithm.setAlgorithm(algorithmMap.get(menuItem.getText()));
						lastSuccessfulItem = menuItem;
					}
				}
			});
			menuItem.setToggleGroup(toggleGroup);
			menu_select_algorithm.getItems().add(menuItem);
		}
		if (menu_select_algorithm.getItems().size() > 0) {
			lastSuccessfulItem = (RadioMenuItem) menu_select_algorithm.getItems().get(0);
			lastSuccessfulItem.setSelected(true);
		}
		List<String> algorithmPatterns = new ArrayList<>();
		for (String algorithmKey : algorithmMap.keySet()) {
			String algorithmPattern = algorithmKey.substring(2, algorithmKey.length()).replace("World", "Patterns");
			algorithmPatterns.add(algorithmPattern);
		}
		List<List<String>> predefinedPatterns = PatternLoader.loadPredefinedPatternList(algorithmPatterns);

		for (int i = 0; i < predefinedPatterns.size(); i++) {
			List<String> predefinedPattern = predefinedPatterns.get(i);
			Menu menu = new Menu(algorithmPatterns.get(i));
			for (String patternName : predefinedPattern) {
				MenuItem menuItem = new MenuItem(patternName);
				menuItem.setOnAction(e -> {
					if (evolutionTaskCancelled) {
						loadWorld(PatternLoader
								.loadPattern(menuItem.getParentMenu().getText() + "/" + menuItem.getText()));

					}
				});

				menu.getItems().add(menuItem);

			}
			menubar.getMenus().add(menubar.getMenus().size() - 1, menu);
			if (i == 0 && predefinedPattern.size() > 0) {
				loadWorld(PatternLoader.loadPattern(algorithmPatterns.get(i) + "/" + predefinedPattern.get(0)));
			}
		}
	}

	/**
	 * Laedt eine Welt in die Canvas. Der bestmoegliche Zoom wird gesetzt.
	 * 
	 * @param world
	 *            vom Typ Boolean[][]
	 */
	private void loadWorld(boolean[][] world) {
		if (world != null && world.length > 0 && world[0].length > 0) {
			setZoomOptimal(world[0].length, world.length);
			mainModel.forceWorld(world);
			updateCanvas();
		}
	}

	/**
	 * Bei Veraenderung des Zooms durch den Benutzer, wird der Zoom neu gesetzt.
	 * Dabei wird die Welt zurueckgesetzt.
	 * 
	 * @param zoom
	 *            vom Typ Integer
	 */
	private void setZoom(int zoom) {
		double canvasWidth = can_draw.getWidth() > 0 ? can_draw.getWidth() : 941;
		double canvasHeight = can_draw.getHeight() > 0 ? can_draw.getHeight() : 640;
		txt_zoom.setText("Zoom: " + zoom);
		canvasRedrawTask.requestRedraw(mainModel.getDrawingModel(true));
		mainModel.setZoom(canvasWidth, canvasHeight, zoom);
		canvasRedrawTask.requestRedraw(mainModel.getDrawingModel(false));
		updateUILabels();
	}

	/**
	 * Die Methode versucht den optimalen Zoom fuer eine hineingeladene Welt zu
	 * finden.
	 * 
	 * @param x
	 *            Breite der neuen Welt
	 * @param y
	 *            Höhe der neuen Welt
	 */
	private void setZoomOptimal(int x, int y) {
		double canvasWidth = can_draw.getWidth() > 0 ? can_draw.getWidth() : 941;
		double canvasHeight = can_draw.getHeight() > 0 ? can_draw.getHeight() : 640;
		canvasRedrawTask.requestRedraw(mainModel.getDrawingModel(true));
		mainModel.setZoomOptimal(canvasWidth, canvasHeight, x, y, false);
		canvasRedrawTask.requestRedraw(mainModel.getDrawingModel(false));
		updateUILabels();
		txt_zoom.setText("Zoom: " + mainModel.getZoom());
	}

	/**
	 * Mit dieser Methode bekommen alle wichtigen Informationsanzeiger in der
	 * GUI während einer Generation die neuesten Informationen
	 */
	private void updateUILabels() {
		txt_size.setText(String.format("Size: %d x %d", mainModel.getSizeX(), mainModel.getSizeY()));
		txt_generation.setText("Generation: " + mainModel.getGeneration());
		txt_population.setText("Population: " + mainModel.getPopulationSize());
		sld_zoom.setValue(mainModel.getZoom());
	}

	/**
	 * Methode, um die Canvas zu aktualisieren. Notwendig, um die im Datenmodell
	 * geaenderte Welt auf die Canvas zu übertragen.
	 */
	private void updateCanvas() {
		setZoom(mainModel.getZoom());
	}

	/**
	 * Methode, die aufgerufen wird, wenn der fortwaehrende 'evolutionTask'
	 * beendet wurde.
	 */
	private void finishedEvolutionTask() {
		mainModel.setIsRunning(false);
	}
}