package game.controller;

import javafx.animation.AnimationTimer;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;

import java.util.concurrent.atomic.AtomicReference;

/**
 * Generische Klasse, die die Canvas neu zeichnet, wenn neue Daten ankommen.
 * Maximal 60x pro Sekunde.
 *
 * @param <T>
 */
public abstract class CanvasRedrawTask<T> extends AnimationTimer {
	private final AtomicReference<T> data = new AtomicReference<T>(null);
	private final Canvas canvas;

	public CanvasRedrawTask(Canvas canvas) {
		this.canvas = canvas;
	}

	public void requestRedraw(T dataToDraw) {
		data.set(dataToDraw);
		start();
	}

	public void handle(long now) {
		T dataToDraw = data.getAndSet(null);
		if (dataToDraw != null) {
			redraw(canvas.getGraphicsContext2D(), dataToDraw);
		}
	}

	protected abstract void redraw(GraphicsContext context, T data);
}