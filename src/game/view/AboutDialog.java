package game.view;

import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Region;
import javafx.stage.Stage;

/**
 * This is an alert dialog that will be shown when the user selects 'About' in
 * the menu. It contains basic information about this software and the
 * programmer of this software.
 *
 */
public class AboutDialog extends Alert {

	/**
	 * Constructor that defines the basic look and feel, title, content and
	 * shown image of this dialog. Except for the typical OK-Button no other
	 * elements are shown.
	 */
	public AboutDialog() {
		super(Alert.AlertType.INFORMATION);
		setTitle("TurtleGames - About");
		setHeaderText("TurtleGames: Game Of Life\nwurde von Andreas und Janek entwickelt.\n\n"
				+ "Dieses Projekt ist im Rahmen eines IT-Talents-Wettbewerbs entstanden.");
		setContentText("			\u00a9 A.T. & J.M.");
		Stage stage = (Stage) this.getDialogPane().getScene().getWindow();
		stage.getIcons().add(new Image("/icon.png"));
		this.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
		ImageView icon = new ImageView("/icon.png");
		icon.setFitWidth(150);
		icon.setFitHeight(150);
		setGraphic(icon);
	}

}