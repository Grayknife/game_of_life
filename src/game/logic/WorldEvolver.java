package game.logic;

import game.model.MainModel;

/**
 * Beinhaltet die Berechnungsregeln, ob eine Zelle lebt oder tot ist.
 * 
 */
public class WorldEvolver {

	private MainModel mainModel;
	private int sizeX;
	private int sizeY;
	private boolean[][] oldWorld;
	private int populationSize;
	private boolean[] born;
	private boolean[] dead;
	private String rule;

	public WorldEvolver(MainModel mainModel) {
		this.mainModel = mainModel;

		born = new boolean[9];
		dead = new boolean[9];
		setAlgorithm("23/3");
	}

	/**
	 * Bestimmt die neue Welt, aufgrund der aktuellen Welt in mainModel und den
	 * Regeln.
	 * 
	 * @return Array[][] vom Typ Boolean
	 */
	public boolean[][] runAlgorithm() {
		sizeX = mainModel.getSizeX();
		sizeY = mainModel.getSizeY();
		populationSize = 0;

		oldWorld = mainModel.getWorld();
		boolean[][] newWorld = new boolean[sizeY][sizeX];

		for (int y = 0; y < sizeY; y++) {
			for (int x = 0; x < sizeX; x++) {
				newWorld[y][x] = isAlive(x, y, oldWorld[y][x]);
			}
		}
		mainModel.setPopulationSize(populationSize);
		return newWorld;
	}

	/**
	 * Berechnet eine spezifische Zelle, in Abhaengigkeit ihrer Position und
	 * ihres aktuellen Status.
	 * 
	 * @param x
	 * @param y
	 * @param status
	 * @return Boolean
	 */
	private boolean isAlive(int x, int y, boolean status) {
		int neighbourSize = countNeighbours(x, y);
		if (neighbourSize >= 0 && neighbourSize < 9) {
			if (born[neighbourSize]) {
				populationSize++;
				return true;
			} else if (dead[neighbourSize]) {
				return false;
			} else {
				populationSize = populationSize + (status ? 1 : 0);
				return status;
			}
		}
		return false;
	}

	/**
	 * Entwickelt nach der eingegebenen Regel den Algorithmus fuer die
	 * Berechnung einer Zelle. Gibt True zurueck, wenn die Umwandlung
	 * erfolgreich war.
	 * 
	 * @param rule
	 * @return True, wenn Umwandlung erfolgreich, False wenn nicht
	 */
	public boolean setAlgorithm(String rule) {
		this.rule = rule;
		for (int i = 0; i < 9; i++) {
			dead[i] = true;
			born[i] = false;
		}

		String[] rulePart = rule.split("/");
		if (rulePart.length == 2) {
			char[] rulePart1 = rulePart[0].toCharArray();
			char[] rulePart2 = rulePart[1].toCharArray();
			for (char c : rulePart1) {
				int numericValue = Character.getNumericValue(c);
				if (numericValue >= 0 && numericValue < 9) {
					dead[numericValue] = false;
				}
			}
			for (char c : rulePart2) {
				int numericValue = Character.getNumericValue(c);
				if (numericValue >= 0 && numericValue < 9) {
					born[numericValue] = true;
				}
			}
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Bestimmt die Anzahl der lebenden Nachbarn.
	 * 
	 * @param x
	 *            -Coordinate
	 * @param y
	 *            -Coordinate
	 * @return Anzahl der lebenden Nachbarn
	 */
	private int countNeighbours(int x, int y) {
		int livingNeighbours = 0;
		if (oldWorld[(y - 1 + sizeY) % sizeY][(x - 1 + sizeX) % sizeX])
			livingNeighbours++;
		if (oldWorld[(y + sizeY) % sizeY][(x - 1 + sizeX) % sizeX])
			livingNeighbours++;
		if (oldWorld[(y + 1 + sizeY) % sizeY][(x - 1 + sizeX) % sizeX])
			livingNeighbours++;
		if (oldWorld[(y - 1 + sizeY) % sizeY][(x + sizeX) % sizeX])
			livingNeighbours++;
		if (oldWorld[(y + 1 + sizeY) % sizeY][(x + sizeX) % sizeX])
			livingNeighbours++;
		if (oldWorld[(y - 1 + sizeY) % sizeY][(x + 1 + sizeX) % sizeX])
			livingNeighbours++;
		if (oldWorld[(y + sizeY) % sizeY][(x + 1 + sizeX) % sizeX])
			livingNeighbours++;
		if (oldWorld[(y + 1 + sizeY) % sizeY][(x + 1 + sizeX) % sizeX])
			livingNeighbours++;
		return livingNeighbours;
	}

	public String getRule() {
		return rule;
	}

}
