package game.model;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 *
 */
public class DrawingModel {
	public BlockingQueue<Tuple<Integer,Integer, Integer>> pointsToRedraw;
	public boolean clear;

	public DrawingModel(){
		pointsToRedraw= new LinkedBlockingQueue<>();
	}
}

