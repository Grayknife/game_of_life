package game.model;

public class MainModel {

	private boolean[][] actualWorld;
	private boolean[][] firstWorld;
	private DrawingModel drawingModel;

	private boolean isRunning;
	private int sizeX;
	private int sizeY;
	private int zoom;
	private int generation;
	private int populationSize;
	private int zoomMaxValue;

	public MainModel() {
		sizeX = 75;
		sizeY = 50;
		zoom = 12;
		generation = 0;
		populationSize = 0;
		zoomMaxValue = 24;

		firstWorld = new boolean[sizeY][sizeX];
		actualWorld = new boolean[sizeY][sizeX];

		createWorld();
	}

	public int getZoom() {
		return zoom;
	}

	public void setZoom(double width, double height, int zoom) {
		this.zoom = zoom;
		sizeX = (int) Math.round(width / zoom);
		sizeY = (int) Math.round(height / zoom);
		if (sizeX > 0 && sizeY > 0) {
			createWorld();
		}
	}

	/**
	 * Berechnet so gut es geht, den perfekten Zoom fuer die zu zeichnende Welt.
	 * @param width
	 * @param height
	 * @param neededSizeX
	 * @param neededSizeY
	 * @param zoomInAllowed
	 */
	public void setZoomOptimal(double width, double height, int neededSizeX, int neededSizeY, boolean zoomInAllowed) {
		int zoomLocal = 0;
		int zoomX = (int) Math.round(width) / neededSizeX;
		int zoomY = (int) Math.round(height) / neededSizeY;
		if (zoomX > zoomMaxValue && zoomY > zoomMaxValue) {
			zoomLocal = zoomMaxValue;
		} else if (zoomX > zoomY) {
			zoomLocal = zoomY;
		} else {
			zoomLocal = zoomX;
		}
		//test if next higher zoom level will also fit the needed size
		if (neededSizeX <= Math.round(width / (zoomLocal + 1)) || neededSizeY <= Math.round(height / (zoomLocal + 1))) {
			zoomLocal++;
		}

		if (zoomInAllowed || zoomLocal < this.zoom) {
			this.zoom = zoomLocal > 0 ? zoomLocal : 1;
		}
		sizeX = (int) Math.round(width / this.zoom);
		sizeY = (int) Math.round(height / this.zoom);
		if (sizeX > 0 && sizeY > 0) {
			createWorld();
		}
	}

	public int getSizeX() {
		return sizeX;
	}

	private void createWorld() {
		if (drawingModel == null) {
			drawingModel = new DrawingModel();
		}
		generation = 0;
		populationSize = 0;
		forceWorld(firstWorld);
	}

	public int getSizeY() {
		return sizeY;
	}

	public boolean[][] getWorld() {
		return actualWorld;
	}

	public void setIsRunning(boolean isRunning) {
		this.isRunning = isRunning;
	}

	public int getGeneration() {
		return generation;
	}

	public int getPopulationSize() {
		return populationSize;
	}

	public void setPopulationSize(int populationSize) {
		this.populationSize = populationSize;
	}

	/**
	 * Versucht die Eingabe-World auf die World im mainModel anzuwenden.
	 * Skaliert gegebenenfalls, wenn die Welt nicht reinpasst.
	 * 
	 * @param forcedWorld
	 */
	public void forceWorld(boolean[][] forcedWorld) {
		int sizeXLocal = sizeX;
		int sizeYLocal = sizeY;
		boolean[][] newWorld;
		boolean actualWorldNotForcedSize = sizeYLocal != forcedWorld.length || sizeXLocal != forcedWorld[0].length;
		boolean newWorldNotActualSize = drawingModel.clear || sizeYLocal != actualWorld.length
				|| sizeXLocal != actualWorld[0].length;
		if (actualWorldNotForcedSize) {
			newWorld = new boolean[sizeYLocal][sizeXLocal];
		} else {
			newWorld = forcedWorld;
		}
		if (newWorldNotActualSize) {
			drawingModel.clear = true;
		}

		for (int y = 0; y < sizeYLocal; y++) {
			for (int x = 0; x < sizeXLocal; x++) {
				//
				if (actualWorldNotForcedSize && y < forcedWorld.length && x < forcedWorld[y].length) {
					newWorld[y][x] = forcedWorld[y][x];
				}
				//
				if (newWorldNotActualSize || this.actualWorld[y][x] != newWorld[y][x]) {
					if (newWorld[y][x]) {
						drawingModel.pointsToRedraw.offer(new Tuple<>(x, y, 2));
					} else if (isRunning && !newWorldNotActualSize) {
						drawingModel.pointsToRedraw.offer(new Tuple<>(x, y, 1));
					} else {
						drawingModel.pointsToRedraw.offer(new Tuple<>(x, y, 0));
					}
				}
			}
		}
		if (!isRunning) {
			firstWorld = newWorld;
		} else {
			generation++;
		}
		this.actualWorld = newWorld;
	}

	/**
	 * Toggelt eine Zelle und uebergibt sie der Queue, der noch zu zeichnenden Pixel.
	 * @param x
	 * @param y
	 */
	public void worldToggleCell(int x, int y) {
		boolean value = false;
		if (y < this.actualWorld.length && x < this.actualWorld[y].length)
			value = this.actualWorld[y][x] = !this.actualWorld[y][x];
		if (isRunning) {
			if (y < firstWorld.length && x < firstWorld[y].length) {
				firstWorld[y][x] = !firstWorld[y][x];
			}
		}
		if (value) {
			drawingModel.pointsToRedraw.offer(new Tuple<>(x, y, 2));
		} else {
			drawingModel.pointsToRedraw.offer(new Tuple<>(x, y, 0));
		}
	}

	/**
	 *  Stellt das DrawingModel zur Verfuegung.
	 * @param clear
	 * @return DrawingModel
	 */
	public DrawingModel getDrawingModel(boolean clear) {

		if (clear) {
			drawingModel.clear = true;
			drawingModel.pointsToRedraw.clear();
		}
		return drawingModel;
	}
}
