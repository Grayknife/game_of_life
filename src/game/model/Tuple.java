package game.model;

public class Tuple<E, F, G> {
	public E xValue;
	public F yValue;
	public G type;
	public Tuple(E e, F f, G g){
		xValue = e;
		yValue = f;
		type = g;
	}
}
